# xbps-dmenu

![xbps-dmenu in action](demo.gif)

A dmenu frontend for xbps, the default package manager on Void Linux.

## Installation

Extract or link these binaries into a directory included in your $PATH variable.

## Usage

Run xbps-dmenu to get a dmenu prompt for xbps.

## Requirements

- dmenu

- xbps

## TODO

- List dependencies of package when installing a package before confirmation


- List dependencies of package when removing a package before confirmation
